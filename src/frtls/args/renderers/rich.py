# -*- coding: utf-8 -*-
from frtls.args.arg import RecordArg, ScalarArg
from rich import box
from rich.table import Table


def to_rich_table(args: RecordArg) -> Table:

    table = Table(box=box.SIMPLE)
    table.add_column("Name", no_wrap=True, style="bold dark_orange")
    table.add_column("Description", no_wrap=False, style="italic", width=20)
    table.add_column("Type", no_wrap=True)
    table.add_column("Required", no_wrap=True)
    table.add_column("Default", no_wrap=True, style="deep_sky_blue4")

    for arg_name, arg_obj in args.childs.items():

        desc = arg_obj.doc.get_short_help()

        if arg_obj.required:
            req = "yes"
        else:
            req = "no"

        if arg_obj.default is not None:
            default = str(arg_obj.default)
        else:
            default = ""

        if isinstance(arg_obj, ScalarArg):
            arg_type = arg_obj.id
        elif hasattr(arg_obj, "_parent_type"):
            parent = arg_obj._parent_type  # type: ignore
            if not isinstance(parent, ScalarArg):
                raise NotImplementedError()
            arg_type = parent.id
        else:
            raise Exception(
                f"Can't determine parent type for: {arg_obj}, this is a bug."
            )

        table.add_row(arg_name, desc, arg_type, req, default)

    return table
