# -*- coding: utf-8 -*-
import logging
from abc import ABCMeta, abstractmethod
from typing import TYPE_CHECKING, Any, Optional

from frtls.doc.explanation import Explanation, SimpleExplanation, to_value_string
from frtls.exceptions import FrklException
from frtls.types.utils import is_instance_or_subclass


if TYPE_CHECKING:
    from frtls.args.arg import Arg

log = logging.getLogger("frtls")


class ArgValue(object):
    def __init__(self, input: Any, arg_renderer: "ArgRenderer"):

        self._input: Any = input
        self._input_set: bool = True
        self._arg_renderer: ArgRenderer = arg_renderer

        self._arg_name = self._arg_renderer.arg_name
        self._arg = self._arg_renderer.arg

        self._processed_input: Any = None
        self._validated_input: Any = None
        self._explained_input: Optional[Explanation] = None

    @property
    def input(self) -> Any:

        if not self._input_set:
            raise FrklException(
                msg=f"Can't explain input for argument '{self._arg_name}'",
                reason="Input not set yet.",
            )
        return self._input

    @input.setter
    def input(self, input: Any):
        self._input = input
        self._input_set = True

    @property
    def explain(self) -> Explanation:

        if self._explained_input is None:
            expl = self._explain_input(
                self.input, self.processed_input, self.validated_input
            )
            if not is_instance_or_subclass(expl, Explanation):
                expl = SimpleExplanation(expl)
            self._explained_input = expl
        return self._explained_input

    @property
    def validated_input(self) -> Any:

        if self._validated_input is None:
            self._validated_input = self._arg.validate(
                self.processed_input, raise_exception=False
            )
        return self._validated_input

    @property
    def processed_input(self) -> Any:

        if self._processed_input is None:
            self._processed_input = self._arg_renderer._process_input(self.input)
        return self._processed_input

    def _explain_input(
        self, orig_input: Any, processed_input: Any, validated_input: Any
    ) -> Any:

        return to_value_string(validated_input)


class ArgRenderer(metaclass=ABCMeta):
    def __init__(self, arg_name: str, arg: "Arg", **kwargs):

        self._arg_name: str = arg_name
        self._arg: "Arg" = arg
        self._rendered_arg: Any = None

    @property
    def arg(self) -> "Arg":
        return self._arg

    @property
    def arg_name(self) -> str:
        return self._arg_name

    @property
    def rendered_arg(self):

        if self._rendered_arg is None:
            self._rendered_arg = self._render_arg()
        return self._rendered_arg

    @abstractmethod
    def _render_arg(self) -> Any:
        pass

    def _process_input(self, input: Any) -> Any:

        return input

    def create_arg_value(self, input: Any) -> ArgValue:

        av = ArgValue(input=input, arg_renderer=self)
        return av


# class ExplanationRenderer(ArgRenderer):
#     def _explain_input(self, arg_name: str, value: Any, arg: "Arg") -> Any:
#
#         validated = arg.validate(value, raise_exception=False)
#
#         result = {"arg_name": arg_name, "value": validated, "input": value, "arg": arg}
#
#         return result
#
#     def _render_arg(self, arg_name: str, arg: "Arg") -> Any:
#
#         raise NotImplementedError()
#
#     def _process_input(self, arg_name: str, value: Any, arg: "Arg") -> Any:
#
#         raise NotImplementedError()
