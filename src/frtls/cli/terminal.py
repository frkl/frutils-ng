# -*- coding: utf-8 -*-
# from: http://granitosaurus.rocks/getting-terminal-size.html
import os
import platform
import sys
from typing import Tuple

import click

from blessed import Terminal


def create_terminal() -> Terminal:

    if hasattr(sys, "frozen") and platform.system() == "Linux":
        return Terminal(kind="linux")
    else:
        return Terminal()


def get_terminal_size(fallback: Tuple = (80, 24)) -> Tuple:
    """Return the terminal size."""

    for i in range(0, 3):
        try:
            columns, rows = os.get_terminal_size(i)
        except (OSError, AttributeError):
            continue
        break
    else:  # set default if the loop completes which means all failed
        columns, rows = fallback
    return columns, rows


def output_to_terminal(line: str, nl: bool = True, no_output: bool = False):
    """Output a string to the terminal.

    This should work on most terminals.
    """

    if no_output:
        return

    click.echo(line.encode("utf-8"), nl=nl)
