# -*- coding: utf-8 -*-
from collections.abc import Mapping, MutableSequence
from typing import Any, Callable, Iterable, Iterator, Optional, Sequence


def ensure_iterable(
    item_or_list: Any,
    convert_none: bool = True,
    sequence_type: Callable = None,
    set_ok=False,
) -> Optional[Iterable]:
    """Ensures an item is of type Sequence.

    If the input is a string or other non-sequence object, it will be wrapped in a list and returned.

    Args:

        - *item_or_list*: the item to ensure is a list
        - *convert_none*: whether to convert None to an empty list
        - *sequenc_type*: the sequence Type to use if the item needs to be wrapped in a Sequence, defaults to `list`
        - *set_ok*: whether type set is ok too

    """

    if item_or_list is None:
        if convert_none:
            if sequence_type is not None:
                return sequence_type()
            else:
                return []
        else:
            return None

    if isinstance(item_or_list, str) or not isinstance(item_or_list, Iterator):
        if set_ok:
            if isinstance(item_or_list, set):
                return item_or_list

        if sequence_type is not None:
            return sequence_type(item_or_list)
        else:
            return [item_or_list]

    return item_or_list


def is_list_of_strings(input_obj: Any) -> bool:
    """Helper method to determine whether an object is a list or tuple of only strings (or string_types).

    Empty lists return `True`.

    Args:
      - *input_obj*: the object in question

    Returns:
      whether or not the object is a list of strings
    """

    if not input_obj and isinstance(input_obj, Sequence):
        return True

    return (
        bool(input_obj)
        and isinstance(input_obj, Sequence)
        and not isinstance(input_obj, str)
        and all(isinstance(item, str) for item in input_obj)
    )


def is_list_of_single_key_dicts(input_obj: Any) -> bool:
    """Helper method to determine whether an object is a sequence that contains only single-key mappings.

    Empty lists return `False`.

    Args:
        - *input_obj*: the object in question

    Returns:
        whether the input object is such a list
    """

    return (
        bool(input_obj)
        and isinstance(input_obj, Sequence)
        and not isinstance(input_obj, str)
        and all((isinstance(item, Mapping) and len(item) == 1) for item in input_obj)
    )


def list_of_dicts_has_key(dicts, key):
    """Return whether a in sequence of dicts at least one has a certain key.

    Any items in the list that are not mappings are ignored.
    """

    if not dicts or not isinstance(dicts, Sequence):
        return False

    for d in dicts:
        if not isinstance(d, Mapping):
            continue
        if key in d.keys():
            return True

    return False


class FrklList(MutableSequence):
    """A list implementation that allows the input to be single-key dicts, which will be transformed on addition.

    This is useful for when using configuration formats like yaml, because I find those lists easier to parse
    when looking at them.

    This implementation also has an option to mix in (or only consist of) 'regular' dict list items. In case one does not
    think the same way as me about the advantages of single-key dict lists.

    In the future, this might be extended to allow for more control as to the schema of the input list, and lets the
    user potentially disable the single-dict items entirely. And maybe add a type-schema for the input items.
    """

    @classmethod
    def from_dict(self, data):

        key = data["key"]
        allow_regular_dict_items = data.get("allow_regular_dict_items", False)

        items = data.get("items", None)
        if isinstance(items, str):
            items = [items]
        elif isinstance(items, Mapping):
            if not allow_regular_dict_items:
                if len(items) != 1:
                    raise ValueError(
                        "Can't create FrklList: non-single key dict items not allowed in this instance."
                    )
            items = [items]
        elif not isinstance(items, Sequence):
            raise ValueError(
                "Items value for FrklList need to be either a string, a valid dict, or a sequence."
            )

        item_schema = data.get("item_schema", None)
        first_item_is_spec = data.get("first_item_is_spec", None)

        return FrklList(
            key=key,
            *items,
            first_item_is_spec=first_item_is_spec,
            item_schema=item_schema,
            allow_regular_dict_items=allow_regular_dict_items,
        )

    def __init__(
        self,
        *items,
        key=None,
        first_item_is_spec=False,
        item_schema=None,
        allow_regular_dict_items=False,
    ):

        if first_item_is_spec:
            if not items:
                raise Exception(
                    "Can't create FrklList: 'first_item_is_spec' specified, but empty list provided."
                )
            spec = items[0]
            items = items[1:]
            spec_key = spec.pop("key", None)
            spec_item_schema = spec.pop("item_schema", None)
            spec_allow = spec.pop("allow_regular_dict_items", None)

            if key is not None and spec_key is not None:
                raise ValueError(f"Duplicate key arguments provided: {key}/{spec_key}")
            elif key is None:
                key = spec_key

            if item_schema is not None and spec_item_schema is not None:
                raise ValueError(
                    f"Duplicate item_schema arguments provided: {item_schema}/{spec_item_schema}"
                )
            elif item_schema is None:
                item_schema = spec_item_schema

            if spec_allow is not None:
                allow_regular_dict_items = spec_allow

        self._key = key
        self._item_schema = item_schema  # not implemented yet
        self._allow_regular_dict_items = allow_regular_dict_items
        self._items = list()
        for i in items:
            item = self._convert(i)
            self._items.append(item)

    def export(self):

        return self._items

    @property
    def key(self):
        return self._key

    def _convert(self, item):

        if not item:
            raise ValueError("Empty list items not allowed.")
        if isinstance(item, str):
            item = {self._key: item}
        elif isinstance(item, Mapping):
            if len(item) == 1 and self.key not in item.keys():
                key, val = next(iter(item.items()))
                if self._key in val.keys():
                    raise ValueError(
                        f"Can't move key '{self._key}' to its value as it already contains that key: {val}"
                    )
                item = val
                item[self._key] = key
            elif not self._allow_regular_dict_items:
                raise ValueError(
                    "Non single-dict or non string items not allowed in this frkl list."
                )
        else:
            raise ValueError(f"Invalid type '{type(item)}' for frkl list item: {item}")

        return item

    def __len__(self):
        return len(self._items)

    def __getitem__(self, index):
        return self._items[index]

    def __delitem__(self, index):
        del self._items[index]

    def insert(self, index, value):
        value = self._convert(value)
        self._items.insert(index, value)

    def __setitem__(self, index, value):
        value = self._convert(value)
        self._items[index] = value

    def __repr__(self):

        return repr(self._items)

    def __str__(self):

        return repr(self._items)
