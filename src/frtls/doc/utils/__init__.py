# -*- coding: utf-8 -*-
from typing import Any, Optional, Union

from frtls.formats.output_formats import serialize
from rich.markdown import Markdown
from rich.style import Style
from rich.syntax import DEFAULT_THEME, Syntax


def create_dict_block(
    _style: Union[str, Style] = "none", _code_theme: str = "monokai", **config: Any
) -> Markdown:

    config_yaml_string = serialize(config, format="yaml", indent=4, strip=True)
    config_markdown_string = f"``` yaml\n{config_yaml_string}\n```"
    config_markdown = Markdown(
        config_markdown_string, style=_style, code_theme=_code_theme, justify="left"
    )

    return config_markdown


def create_dict_element(
    _indent=0,
    _theme: str = DEFAULT_THEME,
    _prefix: Optional[str] = None,
    _postfix: Optional[str] = None,
    **config: Any,
) -> Syntax:

    config_yaml_string = serialize(config, format="yaml", indent=0, strip=True)
    if _prefix:
        config_yaml_string = _prefix + config_yaml_string
    if _postfix:
        config_yaml_string = config_yaml_string + _postfix

    block = Syntax(config_yaml_string, lexer_name="yaml", theme=_theme)

    return block
