# -*- coding: utf-8 -*-
import atexit
import shutil
import tempfile
from pathlib import Path
from typing import Optional, Union

from slugify import slugify


def ensure_folder(path: Union[str, Path], mode=0o750):

    if isinstance(path, str):
        path = Path(path)

    path.mkdir(mode=mode, parents=True, exist_ok=True)


def generate_valid_filename(text, sep="_"):

    file_name = slugify(text, separator=sep)

    return file_name


def create_temp_dir(
    prefix: Optional[str] = None,
    parent_dir: Union[str, Path] = None,
    delete_after_exit: bool = False,
) -> str:

    if prefix:
        tempdir = tempfile.mkdtemp(prefix=f"{prefix}_", dir=parent_dir)
    else:
        tempdir = tempfile.mkdtemp(dir=parent_dir)

    if delete_after_exit:

        def delete_temp_dir():
            shutil.rmtree(tempdir, ignore_errors=True)

        atexit.register(delete_temp_dir)

    return tempdir
