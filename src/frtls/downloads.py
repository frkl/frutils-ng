# -*- coding: utf-8 -*-
import os
import re
from pathlib import Path
from typing import Any, Mapping, Optional, Union

import httpx
from frtls.defaults import DEFAULT_DOWNLOAD_CACHE_BASE
from frtls.exceptions import FrklException


def calculate_cache_location_for_url(
    url: str, postfix: str = None, repl_chars: Optional[str] = None
) -> str:
    """Utility method to get a unique path that can be used for caching a download."""

    if repl_chars is None:
        repl_chars = "[^_\\-A-Za-z0-9.]+"
    if postfix is not None:
        temp = os.path.join(url, postfix)
    else:
        temp = url

    if os.name != "nt":
        repl = os.path.sep
    else:
        repl = "___X___"

    path = re.sub(repl_chars, repl, temp)
    if os.name == "nt":
        path = path.replace("___X___", os.sep)

    return path


def calculate_cache_path(
    base_path: Union[str, Path], url: str, postfix: str = None
) -> str:

    if not isinstance(base_path, str):
        base_path = base_path.resolve().as_posix()
    else:
        base_path = os.path.abspath(base_path)

    cache_path = calculate_cache_location_for_url(url=url, postfix=postfix)

    result = os.path.join(base_path, cache_path)
    return result


def _prepare_cached_url_download(
    url: str,
    update: bool = False,
    cache_base: Union[str, Path] = DEFAULT_DOWNLOAD_CACHE_BASE,
    return_content: bool = False,
    binary_content: bool = False,
) -> Mapping[str, Any]:
    """Downloads a file using a full or abbreviated url.

    Args:
        - *url*: the url
        - *update*: whether to 'force' update the file if it doesn't exist
        - *cache_base*: root of cache directory
        - *return_content*: whether to return the path to the file (False) or it's content (True)

    Returns:
        the path to the downloaded file or it's content (depending on the 'return_content' variable)
    """

    if not isinstance(url, str):
        raise Exception("Url needs to be string")

    rel_cache_path = calculate_cache_location_for_url(url)
    if isinstance(cache_base, str):
        cache_base = Path(cache_base)
    target_file = cache_base / rel_cache_path
    if os.path.exists(target_file) and os.path.isfile(target_file) and not update:
        if return_content:
            if binary_content:
                content: Union[str, bytes] = target_file.read_bytes()
            else:
                content = target_file.read_text()
            return {"content": content}
        else:
            return {"path": target_file}
    elif os.path.exists(target_file) and not os.path.isfile(target_file):
        raise Exception("Target file is not a file: {}".format(target_file))

    cache_base.mkdir(mode=0o700, exist_ok=True, parents=True)
    target_file.parent.mkdir(mode=0o700, exist_ok=True, parents=True)

    return {"url": url, "target_file": target_file, "return_content": return_content}


def download_cached_text_file(
    url: str,
    update: bool = False,
    cache_base: Union[str, Path] = DEFAULT_DOWNLOAD_CACHE_BASE,
    return_content: bool = False,
) -> Union[Path, str]:
    """Downloads a file using a full or abbreviated url.

    Args:
        - *url*: the url
        - *update*: whether to 'force' update the file if it doesn't exist
        - *cache_base*: root of cache directory
        - *return_content*: whether to return the path to the file (False) or it's content (True)

    Returns:
        the path to the downloaded file or it's content (depending on the 'return_content' variable)
    """

    result = _prepare_cached_url_download(
        url=url, update=update, cache_base=cache_base, return_content=return_content
    )
    if "content" in result.keys() or "path" in result.keys():
        if return_content:
            return result["content"]
        else:
            return result["path"]

    return download_text_file(
        url=result["url"],
        target_file=result["target_file"],
        return_content=result["return_content"],
    )


async def download_cached_text_file_async(
    url: str,
    update: bool = False,
    cache_base: Union[str, Path] = DEFAULT_DOWNLOAD_CACHE_BASE,
    return_content: bool = False,
) -> Union[Path, str]:
    """Downloads a file using a full or abbreviated url.

    Args:
        - *url*: the url
        - *update*: whether to 'force' update the file if it doesn't exist
        - *cache_base*: root of cache directory
        - *return_content*: whether to return the path to the file (False) or it's content (True)

    Returns:
        the path to the downloaded file or it's content (depending on the 'return_content' variable)
    """

    result = _prepare_cached_url_download(
        url=url, update=update, cache_base=cache_base, return_content=return_content
    )
    if "content" in result.keys() or "path" in result.keys():
        if return_content:
            return result["content"]
        else:
            return result["path"]

    return await download_text_file_async(
        url=result["url"],
        target_file=result["target_file"],
        return_content=result["return_content"],
    )


async def download_cached_binary_file_async(
    url: str,
    update: bool = False,
    cache_base: Union[str, Path] = DEFAULT_DOWNLOAD_CACHE_BASE,
    return_content: bool = False,
) -> Union[str, bytes]:
    """Downloads a file using a full or abbreviated url.

    Args:
        - *url*: the url
        - *update*: whether to 'force' update the file if it doesn't exist
        - *cache_base*: root of cache directory
        - *return_content*: whether to return the path to the file (False) or it's content (True)

    Returns:
        the path to the downloaded file or it's content (depending on the 'return_content' variable)
    """

    result = _prepare_cached_url_download(
        url=url,
        update=update,
        cache_base=cache_base,
        return_content=return_content,
        binary_content=True,
    )

    if "content" in result.keys() or "path" in result.keys():
        if return_content:
            return result["content"]
        else:
            return result["path"]

    return await download_binary_file_async(
        url=result["url"],
        target_file=result["target_file"],
        return_content=result["return_content"],
    )


async def download_binary_file_async(
    url: str, target_file: str, return_content: bool = False
) -> Union[bytes, str]:

    import httpx

    async with httpx.AsyncClient() as client:
        r = await client.get(url)

    r.raise_for_status()

    content: bytes = r.content

    try:
        from anyio import aopen

        async with await aopen(target_file, "wb") as f:
            await f.write(content)
    except (Exception) as e:
        raise FrklException(
            msg=f"Could not save content from url '{url}' to: {target_file}", parent=e
        )

    if return_content:
        return content
    else:
        return target_file


async def download_text_file_async(
    url: str, target_file: str, return_content: bool = False
) -> str:

    import httpx

    async with httpx.AsyncClient() as client:
        r = await client.get(url)

    r.raise_for_status()
    content = r.text

    try:
        from anyio import aopen

        async with await aopen(target_file, "w") as f:
            await f.write(content)
    except (Exception) as e:
        raise FrklException(
            msg=f"Could not save content from url '{url}' to: {target_file}", parent=e
        )

    if return_content:
        return content
    else:
        return target_file


def download_text_file(
    url: str, target_file: Path, return_content: bool = False
) -> str:

    r = httpx.get(url, verify=True)
    r.raise_for_status()
    data = r.text

    target_file.write_text(data)

    if return_content:
        return data
    else:
        return target_file.resolve().as_posix()
