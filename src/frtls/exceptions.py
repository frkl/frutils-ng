# -*- coding: utf-8 -*-
import logging
from typing import TYPE_CHECKING, Any, Dict, Mapping, Optional, Union


if TYPE_CHECKING:
    from frtls.args.arg import Arg

log = logging.getLogger("frtls")


def generate_exception_dict(exc):

    if hasattr(exc, "root_exc"):
        root_exc = exc.root_exc
        if isinstance(root_exc, FrklException):
            exc = root_exc
        else:
            exc = FrklException(parent=exc)
    if not isinstance(exc, FrklException) and not issubclass(
        exc.__class__, FrklException
    ):
        exc = FrklException(exc)

    result = {"msg": exc.msg, "solution": exc.solution, "reason": exc.reason}
    return result


class FrklException(Exception):
    """Base exception class with a few helper methods to make it easy to display a good error message.

    Args:

        - *msg*: the message to display
        - *solution*: a recommendation to the user to fix or debug the issue
        - *references*: a dict with references to the issue, title/url key-value pairs.
        - *reason*: the reason for the exception
        - *parent*: a parent exception

    """

    def __init__(
        self,
        msg: Union[str, Exception, None] = None,
        solution: str = None,
        references: Dict = None,
        reason: Union[str, Exception] = None,
        parent: Exception = None,
        *args,
    ):

        if isinstance(msg, Exception) or issubclass(msg.__class__, Exception):
            if parent is None:
                parent = msg
            msg = str(msg)

        if msg is None:
            msg = "unspecified internal error"

        super(FrklException, self).__init__(msg, *args)
        self.msg = msg
        self.solution = solution
        self._reason = reason
        self.references = references
        self.parent = parent

    @property
    def reason(self) -> Optional[str]:

        if not self._reason:
            return None

        if isinstance(self._reason, str):
            return self._reason
        else:
            return str(self._reason)

    @property
    def message(self):

        msg = self.msg
        if not msg.endswith("."):
            msg = msg + "."

        msg = msg + "\n"

        if self.reason:
            msg = msg + "\n  Reason: {}".format(self.reason)
        elif self.parent:
            msg_parent = str(self.parent)
            msg = msg + f"\n  Parent error: {msg_parent}"
        if self.solution:
            msg = msg + "\n  Solution: {}".format(self.solution)
        if self.references:
            if len(self.references) == 1:
                url = self.references[list(self.references.keys())[0]]
                msg = msg + "\n  Reference: {}".format(url)
            else:
                msg = msg + "\n  References\n"
                for k, v in self.references.items():
                    msg = msg + "\n    {}: {}".format(k, v)

        return msg.rstrip()

    @property
    def message_short(self):

        msg = self.msg
        if not msg.endswith("."):
            msg = msg + "."

        if self.reason:
            reason = self.reason
            if not reason.endswith("."):
                reason = reason + "."
            msg = f"{msg} {reason}"
        elif self.parent:
            reason = repr(self.parent)
            if not reason.endswith("."):
                reason = reason + "."
            msg = f"{msg} {reason}"
        if self.solution:
            solution = self.solution
            if not solution.endswith("."):
                solution = solution + "."
            msg = f"{msg} {solution}"

        msg = msg.replace("\n", " ")

        return msg

    def __str__(self):

        return self.message_short

    def __repr__(self):
        return self.message


class FrklDevException(FrklException):
    """A class to indicate this is a developer-targeted exception.

    Not implemented yet, but this should also take arguments to provide user-facing output, with instructions to contact developer or similar.
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


class ArgValidationError(FrklException):
    def __init__(
        self,
        arg: "Arg",
        value: Any,
        child_errors: Optional[Mapping[str, "ArgValidationError"]] = None,
        **kwargs,
    ):

        self._arg: "Arg" = arg
        self._value: Any = value
        self._child_errors: Optional[Mapping[str, ArgValidationError]] = child_errors

        if "msg" not in kwargs and child_errors:
            kwargs["msg"] = f"Errors for values: {', '.join(child_errors.keys())}"
            reason = ""
            for arg_name, ce in child_errors.items():
                reason += f"  - {arg_name}: {ce.msg}\n"
            kwargs["reason"] = reason
        super().__init__(**kwargs)


class ArgsValidationError(FrklException):
    def __init__(self, error_args: Mapping[str, ArgValidationError], **kwargs):

        self._error_args: Mapping[str, ArgValidationError] = error_args

        if len(self._error_args) == 1:
            msg = f"Errors validating arg: '{', '.join(self._error_args.keys())}'"
        else:
            msg = f"Errors validating args: {', '.join(self._error_args.keys())}"
        reason = ""
        for arg_name, ce in self._error_args.items():
            reason += f"  - {arg_name}: {ce.msg}\n"
        super().__init__(msg=msg, reason=reason, **kwargs)


class FrklParseException(FrklException):
    """Exception to hold information about a parsing error.

    Args:
        - *content*: the content that failed to be parsed
        - *msg*: the message to display
        - *solution*: a recommendation to the user to fix or debug the issue
        - *references*: a dict with references to the issue, title/url key-value pairs.
        - *reason*: the reason for the exception
        - *parent*: a parent exception
        - *content_origin*: metadata, a reference where the content came from
    """

    def __init__(
        self,
        content: str,
        msg: str = None,
        solution: str = None,
        references: Dict = None,
        reason: Union[str, Exception] = None,
        content_origin: str = None,
        exception_map: Dict[str, Exception] = None,
    ):
        self.content = content
        self.content_origin = content_origin
        self._exception_map = exception_map
        super(FrklParseException, self).__init__(
            msg=msg, solution=solution, references=references, reason=reason
        )


def ensure_frkl_exception(exc) -> FrklException:

    if isinstance(exc, FrklException) or issubclass(exc.__class__, FrklException):
        return exc

    return FrklException(msg=exc)
