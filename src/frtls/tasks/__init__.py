# -*- coding: utf-8 -*-
import inspect
import logging
from abc import ABCMeta, abstractmethod
from collections import OrderedDict
from datetime import datetime
from functools import total_ordering
from typing import Any, Callable, Dict, Iterable, List, Mapping, Optional, Type

from anyio import create_task_group
from frtls.doc.explanation import Explanation
from frtls.tasks.desc import TaskDesc
from frtls.tasks.exceptions import FrklTasksException
from frtls.tasks.explanation import TaskExplanation, TaskResult, TasksResult
from frtls.types.utils import generate_valid_identifier, is_instance_or_subclass


log = logging.getLogger("frtls")


def flatten_tasks(parent: "Tasks") -> List["Task"]:

    result: List["Task"] = []
    for child in parent._children.values():

        if is_instance_or_subclass(child, Tasks):
            c = flatten_tasks(child)  # type: ignore
            result.extend(c)
        else:
            result.append(child)

    return result


@total_ordering  # type: ignore
class Task(metaclass=ABCMeta):
    def __init__(self, **kwargs: Any) -> None:

        self._id = generate_valid_identifier(length_without_prefix=8)
        self._created_time: datetime = datetime.now()

        self._parent_task: Optional[Task] = kwargs.get("parent_task", None)

        self._task_desc: Optional[TaskDesc] = kwargs.get("task_desc", None)
        if self._parent_task:
            if self._task_desc:
                self._task_desc.parent = self._parent_task.task_desc

        self._is_background_task: bool = kwargs.get("background_task", False)

        self._postprocess_task: Optional[PostprocessTask] = None

        self._started: bool = False
        self._finished: bool = False

        self._success: Optional[bool] = None

        self._error: Optional[Exception] = None

        r_type: Optional[Type] = kwargs.get("result_type", None)

        if r_type is None:
            r_type = self.get_result_type()

        self._result: TaskResult = r_type(task=self)

    def get_result_type(self) -> Type:

        if is_instance_or_subclass(self, Tasks):
            r_type: Type = TasksResult
        else:
            r_type = TaskResult

        return r_type

    @property
    def id(self):
        return self._id

    @property
    def task_desc(self) -> TaskDesc:

        if self._task_desc is None:
            self._task_desc = TaskDesc(id=self._id, created=self._created_time)
            if self.parent_task:
                self._task_desc.parent = self.parent_task.task_desc
        return self._task_desc

    @task_desc.setter
    def task_desc(self, task_desc: TaskDesc):
        self._task_desc = task_desc
        if self.parent_task:
            self._task_desc.parent = self.parent_task.task_desc

    @property
    def parent_task(self) -> Optional["Task"]:
        return self._parent_task

    @parent_task.setter
    def parent_task(self, parent: "Task"):

        if self._started:
            raise Exception("Can't set parent task once task has started.")

        # if self._parent_task:
        #     raise Exception(
        #         f"Can't set parent task, task '{self.id}' already has a parent."
        #     )

        # if not is_instance_or_subclass(parent, Tasks):
        #     raise Exception(
        #         f"Can't set parent task '{parent}', not a subclass of 'Tasks'."
        #     )

        self.task_desc.parent = parent.task_desc
        self._parent_task = parent

    def get_msg(self) -> Optional[str]:
        return None

    def explain(self, **kwargs) -> Explanation:

        return TaskExplanation(self, **kwargs)

    def __eq__(self, other):

        if not is_instance_or_subclass(other, Task):
            return False

        return self.id == other.id

    def __hash__(self):

        return hash(self.id)

    def __lt__(self, other):

        if not is_instance_or_subclass(other, Task):
            return False

        return (self.parent_task, self._created_time, self.id) < (
            other.parent_task,
            other._created_time,
            other.id,
        )

    # def run(self, msg: str = None, failed_msg: str = None) -> None:
    #
    #     wrap_async_task(self.run_async, msg, failed_msg)  # type: ignore

    async def run_async(self, msg: str = None, failed_msg: str = None) -> TaskResult:

        if self._started:
            raise Exception("Task already started.")

        self._started = True
        self.task_desc.task_started(msg=msg, failed_msg=failed_msg)

        success = True

        try:
            result = await self.execute()  # type: ignore

            failed = []
            if is_instance_or_subclass(self, Tasks):
                for t in self.children.values():  # type: ignore
                    if t.is_finished and not t.success:
                        failed.append(t)

            if failed:
                raise FrklTasksException(self)

        except Exception as e:
            log.debug(
                f"Error executing task '{self.task_desc.name}': {e}", exc_info=True
            )
            self._error = e
            result = e
            success = False

        if success and self._postprocess_task is not None:
            if is_instance_or_subclass(self, Tasks):
                self._postprocess_task.set_input_tasks(
                    *self.children.values()  # type: ignore
                )  # type: ignore
            else:
                self._postprocess_task.set_input_tasks(self)

            await self._postprocess_task.run_async()
            self._result.result_value = self._postprocess_task.result.result_value
        else:
            self._result.result_value = result

        if not success:
            self.task_desc.task_failed(exception=self._error)
        else:
            self.task_desc.task_finished()
        self.finish(success)

        return self.result

    @abstractmethod
    async def execute(self) -> Any:

        pass

    @property
    def is_finished(self) -> bool:

        return self._finished

    def finish(self, success: bool) -> None:

        if not self._started:
            raise Exception("Can't finish task, not started yet.")

        self._finished = True
        self._success = success

        if self._parent_task and hasattr(self._parent_task, "child_finished"):
            self._parent_task.child_finished(self)  # type: ignore

    def set_postprocess_task(self, task: "PostprocessTask") -> None:

        self._postprocess_task = task
        task.parent_task = self

    @property
    def result(self) -> TaskResult:

        return self._result

    @property
    def success(self) -> bool:
        if not self._finished:
            raise Exception("Can't get success value for task, not finished yet.")
        return self._success  # type: ignore

    @property
    def error(self) -> Optional[Exception]:
        if not self._finished:
            raise Exception("Can't get error value for task, not finished yet.")

        return self._error


class SingleTaskAsync(Task):
    def __init__(
        self,
        func: Callable,
        func_args: Iterable[Any] = [],
        func_kwargs: Mapping[str, Any] = {},
        **kwargs: Any,
    ) -> None:

        if not inspect.iscoroutinefunction(func):
            raise Exception(f"Not an async task: {func}")

        super().__init__(**kwargs)

        self._func: Callable = func
        self._args: Iterable[Any] = func_args
        self._kwargs: Mapping[str, Any] = func_kwargs

    async def execute(self) -> Any:

        result = await self._func(*self._args, **self._kwargs)

        self._func = None  # type: ignore
        self._args = None  # type: ignore
        self._kwargs = None  # type: ignore
        return result


class PostprocessTask(Task):
    def __init__(self, func: Callable, **kwargs):

        if not inspect.iscoroutinefunction(func):
            raise Exception(f"Not an async task: {func}")

        # TODO: check if function takes input_tasks argument
        super().__init__(**kwargs)

        self._func: Callable = func
        self._input_tasks: Optional[Iterable[Task]] = None

    def set_input_tasks(self, *input_tasks: Task):

        self._input_tasks = input_tasks

    async def execute(self) -> Any:

        if self._input_tasks is None:
            ip: Iterable = []
        else:
            ip = self._input_tasks

        result = await self._func(*ip)

        self._func = None  # type: ignore
        return result


class Tasks(Task):
    def __init__(self, **kwargs: Any) -> None:

        self._children: Dict[datetime, Task] = OrderedDict()
        self._children_finished: List[Task] = []

        self._is_open_ended = kwargs.get("open_ended", False)

        super().__init__(**kwargs)
        # self.task_desc.has_childs = True
        # self.task_desc.is_open_ended = self._is_open_ended

    @property
    def children(self) -> Mapping[datetime, Task]:

        return self._children

    def add_task(self, task: Task) -> None:

        if self._started and not self._is_open_ended:

            raise Exception(
                f"Can't add task to tasks '{self.id}', tasks already started and 'open_ended' set to false."
            )

        # TODO: lock for task
        timestamp = datetime.now()
        while timestamp in self._children.keys():
            timestamp = datetime.now()

        self._children[timestamp] = task
        task.parent_task = self

    def child_finished(self, child: Task):

        if child in self._children:
            raise Exception(
                f"Child '{child.id}' already in list of finished child tasks."
            )

        self._children_finished.append(child)

    async def execute(self) -> Any:

        await self.run_children()

        return None

    @abstractmethod
    async def run_children(self) -> None:

        pass


class ParallelTasksAsync(Tasks):
    def __init__(self, **kwargs):

        super().__init__(**kwargs)

    async def run_children(self) -> None:

        async with create_task_group() as tg:
            for child in self._children.values():
                await tg.spawn(child.run_async)

        return None

    # def add_task(self, task: Task) -> None:
    #
    #     if is_instance_or_subclass(task, Tasks):
    #         for t in flatten_tasks(task):  # type: ignore
    #             super().add_task(t)
    #     else:
    #         super().add_task(task)


class FlattenParallelTasksAsync(Tasks):
    def __init__(self, **kwargs):

        super().__init__(**kwargs)

    async def run_children(self) -> None:

        async with create_task_group() as tg:

            for child in flatten_tasks(self):
                await tg.spawn(child.run_async)

        return

    def add_task(self, task: Task) -> None:

        if is_instance_or_subclass(task, Tasks):
            for t in flatten_tasks(task):  # type: ignore
                super().add_task(t)
        else:
            super().add_task(task)


class SerialTasksAsync(Tasks):
    def __init__(self, **kwargs):

        super().__init__(**kwargs)

    async def run_children(self) -> None:

        for child in self._children.values():
            await child.run_async()

        return None


class FlattenSerialTasksAsync(Tasks):
    def __init__(self, **kwargs):

        super().__init__(**kwargs)

    async def run_children(self) -> None:

        for child in flatten_tasks(self):
            await child.run_async()

        return None
