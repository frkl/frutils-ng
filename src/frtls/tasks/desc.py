# -*- coding: utf-8 -*-
from abc import ABCMeta, abstractmethod
from datetime import datetime
from typing import Any, Dict, List, Mapping, Optional

from frtls.types.utils import generate_valid_identifier
from pubsub import pub


class TaskDescPlugin(metaclass=ABCMeta):
    @abstractmethod
    def task_started(self, task_desc: "TaskDesc") -> None:
        pass

    @abstractmethod
    def task_finished(self, task_desc: "TaskDesc") -> None:
        pass

    @abstractmethod
    def task_failed(self, task_desc: "TaskDesc") -> None:
        pass


class PubSubTaskDescPlugin(TaskDescPlugin):
    def __init__(self, base_topic: str = "tasks"):

        self._base_topic = base_topic

    def task_started(self, task_desc: "TaskDesc") -> None:
        pub.sendMessage(
            self._base_topic,
            event_name="task_started",
            source=task_desc,
            event_details={},
        )

    def task_finished(self, task_desc: "TaskDesc") -> None:

        pub.sendMessage(
            self._base_topic,
            event_name="task_finished",
            source=task_desc,
            event_details={},
        )

    def task_failed(self, task_desc: "TaskDesc") -> None:

        pub.sendMessage(
            self._base_topic,
            event_name="task_failed",
            source=task_desc,
            event_details={},
        )


class TaskDesc(object):
    def __init__(self, **kwargs: Any) -> None:

        self._name: Optional[str] = kwargs.get("name", None)
        if self._name is None:
            prefix = self.__class__.__name__
        else:
            prefix = self._name
        self._id: str = kwargs.get(
            "id",
            generate_valid_identifier(prefix=f"{prefix}_", length_without_prefix=8),
        )

        self._created_time: datetime = kwargs.get("created", datetime.now())
        self._parent: Optional[TaskDesc] = kwargs.get("parent", None)

        self._has_childs: Optional[bool] = kwargs.get("has_childs", None)
        self._open_ended: Optional[bool] = kwargs.get("open_ended", None)

        self._msg: Optional[str] = kwargs.get("msg", None)
        self._failed_msg: Optional[str] = kwargs.get("failed_msg", None)
        self._result_msg: Optional[str] = None
        self._exception: Optional[Exception] = None

        self._sub_tasks: Mapping[datetime, TaskDesc] = {}

        self._start_time: Optional[datetime] = None
        self._end_time: Optional[datetime] = None

        self._failed: bool = False

        self._topic: Optional[str] = None

        basetopic = kwargs.get("basetopic", None)
        subtopic = kwargs.get("subtopic", None)

        self._basetopic: Optional[str] = basetopic
        self._subtopic: Optional[str] = subtopic
        self._plugins: Dict[str, TaskDescPlugin] = {}

        self._set_topic()

    @property
    def id(self) -> str:
        return self._id

    @property
    def name(self) -> str:
        if not self._name:
            return self._id
        return self._name

    @name.setter
    def name(self, name: str) -> None:
        self._name = name

    @property
    def basetopic(self) -> Optional[str]:
        return self._base_topic

    @basetopic.setter
    def basetopic(self, base_topic: str) -> None:

        self._base_topic = base_topic
        self._set_topic()

    @property
    def subtopic(self) -> Optional[str]:
        return self._subtopic

    @subtopic.setter
    def subtopic(self, subtopic: str) -> None:
        self._subtopic = subtopic
        self._set_topic()

    @property
    def topic(self) -> Optional[str]:

        return self._topic

    def _set_topic(self):

        old_topic = self._topic

        if self._basetopic is None:
            new_topic = None
        else:
            if self._subtopic is None:
                new_topic = self._basetopic
            else:
                new_topic = f"{self._basetopic}.{self._subtopic}"

        if old_topic == new_topic:
            return

        if old_topic is not None:
            self._plugins.pop(old_topic)

        if new_topic is not None:
            self._plugins[new_topic] = PubSubTaskDescPlugin(base_topic=new_topic)

        self._topic = new_topic

    @property
    def parent(self) -> Optional["TaskDesc"]:

        return self._parent

    @parent.setter
    def parent(self, parent: "TaskDesc") -> None:

        if self._start_time:
            raise Exception("Can't set parent after task started.")

        # if self._parent:
        #     raise Exception(f"Task desc already has parent set: {self._id}")
        self._parent = parent

    # def add_plugins(self, *plugins: TaskDescPlugin) -> None:
    #
    #     for p in plugins:
    #         self._plugins.append(p)

    @property
    def msg(self) -> Optional[str]:

        if self._msg is None:
            return f"executing: {self.name}"

        return self._msg

    @msg.setter
    def msg(self, msg: str) -> None:

        if self._msg:
            raise Exception(
                f"Message already set for task desc '{self.name}': '{self.msg}'"
            )

        self._msg = msg

    def get_failed_msg(self, default: str = None) -> str:

        if self._failed_msg is None:
            if self._exception is not None:
                return str(self._exception)
            else:
                if default is None:
                    return f"failed: {self.name}"
                else:
                    return default

        return self._failed_msg

    def set_failed_msg(self, msg: str) -> None:

        if self._failed_msg:
            raise Exception(
                f"Failed message already set for task desc '{self.name}': '{self.msg}'"
            )

        self._failed_msg = msg

    def get_result_msg(self, default: str = None) -> str:

        if self._result_msg is None:
            if default is None:
                return f"finished: {self.name}"
            else:
                return default

        return self._result_msg

    @property
    def namespace(self) -> str:

        if not self._start_time:
            raise Exception("Can't get namespace before task has started.")

        current_parent: Optional[TaskDesc] = self.parent

        tokens: List[str] = []
        while True:
            if current_parent:
                tokens.insert(0, current_parent.id)
            else:
                break
            current_parent = self.parent.parent  # type: ignore

        return ".".join(tokens)

    def task_started(self, msg: str = None, failed_msg: str = None) -> None:

        self._start_time = datetime.now()
        if msg is not None:
            self.msg = msg
        if failed_msg is not None:
            self.set_failed_msg(failed_msg)

        for plugin in self._plugins.values():
            plugin.task_started(self)

    def task_finished(self, msg: str = None) -> None:

        self._end_time = datetime.now()
        self._result_msg = msg

        for plugin in self._plugins.values():
            plugin.task_finished(self)

    def is_started(self) -> bool:

        return self._start_time is not None

    def is_finished(self) -> bool:

        return self._end_time is not None

    def task_failed(self, failed_msg: str = None, exception: Exception = None) -> None:

        self._end_time = datetime.now()
        if failed_msg is not None:
            self._failed_msg = failed_msg
        self._exception = exception

        for plugin in self._plugins.values():
            plugin.task_failed(self)

    def __repr__(self):

        return (
            f"[{self.__class__.__name__}: name={self.name} msg={self.msg} id={self.id}"
        )
