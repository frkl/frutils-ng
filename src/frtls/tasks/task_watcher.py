# -*- coding: utf-8 -*-
import atexit
import collections
import logging
import uuid
from abc import ABCMeta, abstractmethod
from typing import Any, Dict, Iterable, List, Mapping, Optional, Union

from frtls.exceptions import FrklException
from frtls.tasks import TaskDesc
from frtls.types.plugins import TypistryPluginManager
from frtls.types.typistry import Typistry
from frtls.types.utils import is_instance_or_subclass
from pubsub import pub


log = logging.getLogger("frtls")

DEFAULT_TOPIC = "frtls.tasks"


class TaskWatcher(metaclass=ABCMeta):

    _plugin_type = "instance"

    def __init__(
        self,
        base_topics: Optional[Iterable[str]] = None,
        root_task: Optional[TaskDesc] = None,
    ):

        if not base_topics:
            base_topics = []

        self._base_topics: Iterable[str] = base_topics

        self._root_task: Optional[TaskDesc] = root_task
        self._started: bool = True

        self.start()

    @property
    def started(self) -> bool:

        return self._started

    def start(self):

        for bt in self._base_topics:
            pub.subscribe(self.task_handler, bt)

        self._started = True

    def stop(self):

        for bt in self._base_topics:
            pub.unsubscribe(self.task_handler, bt)
        self._started = False

    def task_handler(
        self, event_name: str, source: Any, event_details: Mapping[str, Any]
    ):

        if not self._started:
            return

        if event_name == "task_started":
            self._task_started(source, event_details)
        elif event_name == "task_finished":
            self._task_finished(source, event_details)
        elif event_name == "task_failed":
            self._task_failed(source, event_details)

    @abstractmethod
    def _task_started(self, source: TaskDesc, event_details: Mapping[str, Any]):

        pass

    @abstractmethod
    def _task_finished(self, source: TaskDesc, event_details: Mapping[str, Any]):

        pass

    @abstractmethod
    def _task_failed(self, source: TaskDesc, event_details: Mapping[str, Any]):

        pass


def create_watcher_obj(
    plugin_manager: TypistryPluginManager,
    watcher: Union[TaskWatcher, str, Mapping[str, Any]],
    root_task: Optional[TaskDesc],
):

    if is_instance_or_subclass(watcher, TaskWatcher):
        _watcher_obj: TaskWatcher = watcher  # type: ignore
    else:
        if isinstance(watcher, str):
            _watcher: Dict[str, Any] = {"type": watcher}
        elif isinstance(watcher, collections.abc.Mapping):
            _watcher = dict(watcher)
        else:
            raise FrklException(
                msg="Can't add watcher.",
                reason=f"Invalid watcher config type '{type(watcher)}': {watcher}",
            )

        _watcher["root_task"] = root_task

        watcher_type = _watcher.pop("type")

        if watcher_type == "default":
            watcher_type = "tree"

        plugin = plugin_manager.get_plugin(watcher_type)
        if plugin is None:
            raise FrklException(
                msg=f"Can't create task watcher of type '{watcher_type}'.",
                reason="No plugin class available.",
            )

        _watcher_obj = plugin(**_watcher)

    return _watcher_obj


class TaskWatchManager(object):
    def __init__(
        self,
        root_task: Optional[TaskDesc] = None,
        typistry: Typistry = None,
        watchers: Optional[Iterable[Union[str, Mapping[str, Any]]]] = None,
    ):

        if typistry is None:
            typistry = Typistry()
        self._typistry = typistry
        self._plugin_manager = self._typistry.get_plugin_manager(
            TaskWatcher, _manager_name="task_watchers"
        )
        self._watchers: Dict[str, TaskWatcher] = {}

        self._root_task: Optional[TaskDesc] = root_task

        if watchers:
            for w in watchers:
                self.add_watcher(w, root_task=root_task)

        if self._root_task:
            self._root_task.task_started()

            def finish_task():
                if not self._root_task.is_finished():
                    self._root_task.task_finished()

            atexit.register(finish_task)

    @property
    def available_plugins(self) -> Iterable[str]:

        return self._plugin_manager.plugin_names

    def add_watchers(
        self, *watchers: Union[str, Mapping[str, Any]], root_task: TaskDesc = None
    ) -> List[str]:

        ids = []
        for w in watchers:
            id = self.add_watcher(watcher=w, root_task=root_task)
            ids.append(id)

        return ids

    def add_watcher(
        self,
        watcher: Union[str, Mapping[str, Any], TaskWatcher],
        watcher_id: Optional[str] = None,
        root_task: Optional[TaskDesc] = None,
    ) -> str:

        if watcher_id is None:
            watcher_id = str(uuid.uuid4())

        _watcher_obj = create_watcher_obj(
            self._plugin_manager, watcher, root_task=root_task
        )

        # if is_instance_or_subclass(watcher, TaskWatcher):
        #     _watcher_obj: TaskWatcher = watcher  # type: ignore
        # else:
        #     if isinstance(watcher, str):
        #         _watcher: Dict[str, Any] = {"type": watcher}
        #     elif isinstance(watcher, collections.abc.Mapping):
        #         _watcher = dict(watcher)
        #     else:
        #         raise FrklException(
        #             msg=f"Can't add watcher with id: {watcher_id}",
        #             reason=f"Invalid watcher config type '{type(watcher)}': {watcher}",
        #         )
        #
        #     _watcher["root_task"] = root_task
        #
        #     watcher_type = _watcher.pop("type")
        #
        #     if watcher_type == "default":
        #         watcher_type = "tree"
        #
        #     plugin = self._plugin_manager.get_plugin(watcher_type)
        #     if plugin is None:
        #         raise FrklException(
        #             msg=f"Can't create task watcher of type '{watcher_type}'.",
        #             reason="No plugin class available.",
        #         )
        #
        #     _watcher_obj = plugin(**_watcher)

        self._watchers[watcher_id] = _watcher_obj
        return watcher_id

    def get_watcher(self, id: str) -> Optional[TaskWatcher]:

        return self._watchers.get(id, None)

    def remove_watcher(self, watcher_id: str):

        watcher = self._watchers.get(watcher_id, None)
        if watcher is None:
            log.debug(f"No watcher with id '{watcher_id}', doing nothing...")
            return

        watcher.stop()
        self._watchers.pop(watcher_id)
