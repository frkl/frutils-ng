# -*- coding: utf-8 -*-
from typing import TYPE_CHECKING, Any, Mapping

from frtls.doc.explanation import Explanation


if TYPE_CHECKING:
    from frtls.targets import Target, TargetItem


class TargetItemExplanation(Explanation):
    def __init__(self, data: "TargetItem"):

        super().__init__(data=data)

    async def create_explanation_data(self) -> Mapping[str, Any]:

        result = {}
        result["id"] = self.data.id
        result["metadata"] = await self.data.get_metadata()

        return result


class TargetExplanation(Explanation):
    def __init__(self, data: "Target"):

        super().__init__(data=data)

    async def create_explanation_data(self) -> Mapping[str, Any]:

        items = await self.data.get_items()

        return {"managed_items": items.values()}
