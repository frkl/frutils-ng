# -*- coding: utf-8 -*-
from typing import Any, Dict, Iterable, Type

from frtls.defaults import DEFAULT_PLUGIN_MANAGER_CONFIG
from frtls.dicts import get_seeded_dict
from frtls.doc.doc import Doc
from frtls.exceptions import FrklException
from frtls.lists import ensure_iterable
from frtls.types.utils import get_type_alias


class TypistryPluginManager(object):
    def __init__(self, base_class: Type, *plugins: Type, manager_name: str, **config):

        self._name = manager_name

        self._base_class: Type = base_class
        self._plugins: Dict[str, Type] = {}
        self._supports_map: Dict[str, Type] = {}
        self._config = get_seeded_dict(DEFAULT_PLUGIN_MANAGER_CONFIG, config)

        if hasattr(self._base_class, "_plugin_type"):
            plugin_type = self._base_class._plugin_type
        else:
            plugin_type = "instance"

        if plugin_type not in ["singleton", "instance"]:
            raise ValueError(f"Invalid plugin type: {plugin_type}, this is a bug.")

        self._plugin_type = plugin_type
        self._name_attribute = self._config["name_attribute"]
        self._supports_attribute = self._config["supports_attribute"]

        self._plugin_config = self._config.get("plugin_config", {})

        for plugin in plugins:
            self.add_plugin(plugin)

    @property
    def manager_name(self) -> str:

        return self._name

    @property
    def plugin_names(self) -> Iterable[str]:

        return self._plugins.keys()

    def add_plugin(self, plugin: Type):

        try:
            if self._plugin_type == "singleton":

                # print(plugin)
                # print(self._plugin_config)
                # sig = inspect.signature(plugin.__init__)
                #
                # print(sig.parameters)
                # if "config" in sig.parameters:
                #     plugin_obj = plugin(config=self._plugin_config)
                # else:
                plugin_obj = plugin(**self._plugin_config)

                if hasattr(plugin_obj, self._name_attribute):
                    plugin_name = getattr(plugin_obj, self._name_attribute)
                else:
                    plugin_name = get_type_alias(plugin, include_module_path=False)
                self._plugins[plugin_name] = plugin_obj

                if hasattr(plugin_obj, self._supports_attribute):
                    supports = getattr(plugin_obj, self._supports_attribute)
                    for s in ensure_iterable(supports):  # type: ignore
                        s = s.replace("-", "_")
                        self._supports_map[s] = plugin_obj
                else:
                    plugin_name = plugin_name.replace("-", "_")
                    self._supports_map[plugin_name] = plugin_obj
            elif self._plugin_type == "instance":
                if hasattr(plugin, self._name_attribute):
                    plugin_name = getattr(plugin, self._name_attribute)
                else:
                    plugin_name = get_type_alias(plugin, include_module_path=False)

                plugin_name = plugin_name.replace("-", "_")
                self._plugins[plugin_name] = plugin

                if hasattr(plugin, self._supports_attribute):
                    supports = getattr(plugin, self._supports_attribute)
                    for s in ensure_iterable(supports):  # type: ignore
                        s = s.replace("-", "_")
                        self._supports_map[s] = plugin
                else:
                    self._supports_map[plugin_name] = plugin

            else:
                raise NotImplementedError()
        except Exception as e:
            raise FrklException(msg=f"Can't add plugin '{plugin.__name__}'", parent=e)

    def get_plugin_class(self, plugin_name: str) -> Type:

        if self._plugin_type == "singleton":
            plugin_class = self.get_plugin(
                plugin_name=plugin_name, raise_exception=True
            ).__class__
        else:
            plugin_class = self.get_plugin(plugin_name, raise_exception=True)

        return plugin_class

    def get_plugin_doc(self, plugin_name: str) -> Doc:

        plugin_class = self.get_plugin_class(plugin_name=plugin_name)
        doc = Doc.from_docstring(plugin_class)
        return doc

    def get_plugin(self, plugin_name: str, raise_exception: bool = False) -> Any:

        plugin_name = plugin_name.replace("-", "_")

        result = self._plugins.get(plugin_name, None)
        if result is None and raise_exception:
            if self.plugin_names:
                avail = ", ".join(self.plugin_names)
            else:
                avail = "none"
            raise FrklException(
                msg=f"Can't retieve plugin '{plugin_name}' for '{self._name}'.",
                reason=f"No such plugin registered, available plugins: {avail}",
                solution="Make sure all modules that include plugin classes are loaded, and all required plugins implement all abstract methods.",
            )

        return result

    def get_plugin_for(self, item_type: str, raise_exception: bool = False):

        item_type = item_type.replace("-", "_")
        result = self._supports_map.get(item_type, None)
        if result is None and raise_exception:
            raise FrklException(
                msg=f"Can't retieve plugin for '{item_type}' amongst plugins for '{self._name}'.",
                reason="No suitable plugin registered.",
            )

        return result
