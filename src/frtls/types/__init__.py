# -*- coding: utf-8 -*-
import logging
from typing import Dict, Type


log = logging.getLogger("frtls")


class Singleton(type):

    _instances: Dict[Type, Type] = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        else:
            cls._instances[cls].__init__(*args, **kwargs)

        return cls._instances[cls]
